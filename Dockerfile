# Use the latest stable fedora base
FROM fedora:35

# Ensure all the packages are up to date and install dependencies. Performed in one line to force full upgrade on change
RUN dnf upgrade -y && \
    dnf install -y \
        latexmk \
        texlive-acronym \
        texlive-appendix \
        texlive-cleveref \
        texlive-epstopdf \
        texlive-IEEEtran \
        texlive-minted \
        texlive-newfile \
        texlive-quantikz \
        texlive-scheme-medium \
        texlive-xurl && \
    dnf clean all
